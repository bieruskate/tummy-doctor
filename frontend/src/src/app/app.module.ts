import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { IllnessFormComponent } from './illness-form/illness-form.component';

import { MatSelectModule, MatTabsModule, MatCardModule, MatRadioModule, MatButtonModule, MatDialogModule, MatStepperModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AboutComponent } from './about/about.component';
import { IllnessService } from './services/illness.service';
import { HttpModule } from '@angular/http';
import { DiagnoseDialogComponent } from './diagnose-dialog/diagnose-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    IllnessFormComponent,
    AboutComponent,
    DiagnoseDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTabsModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatStepperModule,
    MatProgressSpinnerModule
  ],
  providers: [
    IllnessService
  ],
  bootstrap: [AppComponent],
  entryComponents: [DiagnoseDialogComponent]
})
export class AppModule { }
