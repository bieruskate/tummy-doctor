export const q0 = {
    displayName: 'Płeć',
    options: [
        { value: 1, viewValue: 'Męska' },
        { value: 2, viewValue: 'Żeńska' },
    ]
};

export const q1 = {
    displayName: 'Wiek',
    options: [
        { value: 1, viewValue: 'Poniżej 20 lat' },
        { value: 2, viewValue: '20 - 30 lat' },
        { value: 3, viewValue: '31 - 40 lat' },
        { value: 4, viewValue: '41 - 50 lat' },
        { value: 5, viewValue: 'Powyżej 50 lat' },
    ]
};

export const q2 = {
    displayName: 'Lokalizacja bólu na początku zachorowania',
    options: [
        { value: 1, viewValue: 'Prawa Górna Ćwiartka' },
        { value: 2, viewValue: 'Lewa Górna Ćwiartka' },
        { value: 3, viewValue: 'Górna Połowa' },
        { value: 4, viewValue: 'Prawa Połowa' },
        { value: 5, viewValue: 'Lewa Połowa' },
        { value: 6, viewValue: 'Centralny Kwadrat' },
        { value: 7, viewValue: 'Cały Brzuch' },
        { value: 8, viewValue: 'Prawa Dolna Ćwiartka' },
        { value: 9, viewValue: 'Lewa Dolna Ćwiartka' },
        { value: 10, viewValue: 'Dolna Połowa' },
    ]
};

export const q3 = {
    displayName: 'Lokalizacja bólu obecnie',
    options: [
        { value: 0, viewValue: 'Brak Bólu' },
        { value: 1, viewValue: 'Prawa Górna Ćwiartka' },
        { value: 2, viewValue: 'Lewa Górna Ćwiartka' },
        { value: 3, viewValue: 'Górna Połowa' },
        { value: 4, viewValue: 'Prawa Połowa' },
        { value: 5, viewValue: 'Lewa Połowa' },
        { value: 6, viewValue: 'Centralny Kwadrat' },
        { value: 7, viewValue: 'Cały Brzuch' },
        { value: 8, viewValue: 'Prawa Dolna Ćwiartka' },
        { value: 9, viewValue: 'Lewa Dolna Ćwiartka' },
        { value: 10, viewValue: 'Dolna Połowa' },
    ]
};

export const q4 = {
    displayName: 'Intensywność bólu',
    options: [
        { value: 0, viewValue: 'Łagodny/Brak' },
        { value: 1, viewValue: 'Umiarkowany' },
        { value: 2, viewValue: 'Silny' },
    ]
};

export const q5 = {
    displayName: 'Czynniki nasilające ból',
    options: [
        { value: 0, viewValue: 'Brak Czynników' },
        { value: 1, viewValue: 'Oddychanie' },
        { value: 2, viewValue: 'Kaszel' },
        { value: 3, viewValue: 'Ruchy Ciała' },
    ]
};

export const q6 = {
    displayName: 'Czynniki przynoszące ulgę',
    options: [
        { value: 0, viewValue: 'Brak Czynników' },
        { value: 1, viewValue: 'Wymioty' },
        { value: 2, viewValue: 'Pozycja Ciała' },
    ]
};

export const q7 = {
    displayName: 'Progresja bólu',
    options: [
        { value: 1, viewValue: 'Ustepujący' },
        { value: 2, viewValue: 'Bez Zmian' },
        { value: 3, viewValue: 'Nasilający Się' },
    ]
};

export const q8 = {
    displayName: 'Czas trwania bólu',
    options: [
        { value: 1, viewValue: 'Mniej Niż 12 Godzin' },
        { value: 2, viewValue: '12 - 24 Godzin' },
        { value: 3, viewValue: '24 - 48 Godzin' },
        { value: 4, viewValue: 'Powyżej 48 Godzin' },
    ]
};

export const q9 = {
    displayName: 'Charakter bólu na początku zachorowania',
    options: [
        { value: 1, viewValue: 'Przerywany' },
        { value: 2, viewValue: 'Stały' },
        { value: 3, viewValue: 'Kolkowy' },
    ]
};

export const q10 = {
    displayName: 'Charakter bólu obecnie',
    options: [
        { value: 0, viewValue: 'Brak Bólu' },
        { value: 1, viewValue: 'Przerywany' },
        { value: 2, viewValue: 'Stały' },
        { value: 3, viewValue: 'Kolkowy' },
    ]
};

export const q11 = {
    displayName: 'Nudności i wymioty',
    options: [
        { value: 0, viewValue: 'Brak' },
        { value: 1, viewValue: 'Nudności Bez Wymiotów' },
        { value: 2, viewValue: 'Nudności Z Wymiotami' },
    ]
};

export const q12 = {
    displayName: 'Apetyt',
    options: [
        { value: 1, viewValue: 'Zmniejszony' },
        { value: 2, viewValue: 'Normalny' },
        { value: 3, viewValue: 'Zwiększony' },
    ]
};

export const q13 = {
    displayName: 'Wypróżnienia',
    options: [
        { value: 1, viewValue: 'Biegunki' },
        { value: 2, viewValue: 'Prawidłowe' },
        { value: 3, viewValue: 'Zaparcia' },
    ]
};

export const q14 = {
    displayName: 'Oddawanie moczu',
    options: [
        { value: 1, viewValue: 'Normalne' },
        { value: 2, viewValue: 'Dysuria' },
    ]
};

export const q15 = {
    displayName: 'Poprzednie niestrawności',
    options: [
        { value: 0, viewValue: 'Nie' },
        { value: 1, viewValue: 'Tak' },
    ]
};

export const q16 = {
    displayName: 'Żółtaczka w przeszłości',
    options: [
        { value: 0, viewValue: 'Nie' },
        { value: 1, viewValue: 'Tak' },
    ]
};

export const q17 = {
    displayName: 'Poprzednie operacje brzuszne',
    options: [
        { value: 0, viewValue: 'Nie' },
        { value: 1, viewValue: 'Tak' },
    ]
};

export const q18 = {
    displayName: 'Leki',
    options: [
        { value: 0, viewValue: 'Nie' },
        { value: 1, viewValue: 'Tak' },
    ]
};

export const q19 = {
    displayName: 'Stan psychiczny',
    options: [
        { value: 1, viewValue: 'Pobudzony/Cierpiący' },
        { value: 2, viewValue: 'Prawidłowy' },
        { value: 3, viewValue: 'Apatyczny' },
    ]
};

export const q20 = {
    displayName: 'Skóra',
    options: [
        { value: 1, viewValue: 'Blada' },
        { value: 2, viewValue: 'Prawidłowa' },
        { value: 3, viewValue: 'Zaczerwieniona (Twarz)' },
    ]
};

export const q21 = {
    displayName: 'Temperatura (pacha)',
    options: [
        { value: 1, viewValue: 'Poniżej 36.5°C' },
        { value: 2, viewValue: '36.5 - 37°C' },
        { value: 3, viewValue: '37 - 37.5°C' },
        { value: 4, viewValue: '37.5 - 38°C' },
        { value: 5, viewValue: '38 - 39°C' },
        { value: 6, viewValue: 'Powyżej 39°C' },
    ]
};

export const q22 = {
    displayName: 'Ruchy oddechowe powłok brzusznych',
    options: [
        { value: 1, viewValue: 'Normalne' },
        { value: 2, viewValue: 'Zniesione' },
    ]
};

export const q23 = {
    displayName: 'Wzdęcia',
    options: [
        { value: 0, viewValue: 'Nie' },
        { value: 1, viewValue: 'Tak' },
    ]
};

export const questions = [q0, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23];

export const overallQuestions = [q0, q1];
export const painQuestions = [q2, q3, q4, q5, q6, q7, q8, q9, q10];
export const otherQuestions = [q11, q12, q13, q14];
export const historyQuestions = [q15, q16, q17, q18];
export const examinationQuestions = [q19, q20, q21];
export const stomachQuestions = [q22, q23];
