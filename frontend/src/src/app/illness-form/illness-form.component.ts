import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { overallQuestions, painQuestions, otherQuestions, historyQuestions, examinationQuestions, stomachQuestions } from './questions';
import { IllnessService } from '../services/illness.service';
import { MatDialog } from '@angular/material';
import { DiagnoseDialogComponent } from '../diagnose-dialog/diagnose-dialog.component';

@Component({
  selector: 'app-illness-form',
  templateUrl: './illness-form.component.html',
  styleUrls: ['./illness-form.component.css']
})
export class IllnessFormComponent {
  overallQuestions = overallQuestions;
  overallForm: FormGroup;
  painQuestions = painQuestions;
  painForm: FormGroup;
  otherQuestions = otherQuestions;
  otherForm: FormGroup;
  historyQuestions = historyQuestions;
  historyForm: FormGroup;
  examinationQuestions = examinationQuestions;
  examinationForm: FormGroup;
  stomachQuestions = stomachQuestions;
  stomachForm: FormGroup;

  displaySpinner = false;

  constructor(public dialog: MatDialog, private fb: FormBuilder, private service: IllnessService) {
    this.createForm();
  }

  createForm() {
    let controls = this.createControls(this.overallQuestions);
    this.overallForm = this.fb.group(controls);

    controls = this.createControls(this.painQuestions);
    this.painForm = this.fb.group(controls);

    controls = this.createControls(this.otherQuestions);
    this.otherForm = this.fb.group(controls);

    controls = this.createControls(this.historyQuestions);
    this.historyForm = this.fb.group(controls);

    controls = this.createControls(this.examinationQuestions);
    this.examinationForm = this.fb.group(controls);

    controls = this.createControls(this.stomachQuestions);
    this.stomachForm = this.fb.group(controls);
  }

  areFormsValid() {
    return this.overallForm.valid && this.painForm.valid && this.otherForm.valid &&
      this.historyForm.valid && this.examinationForm.valid && this.stomachForm.valid;
  }

  createControls(params: any[]) {
    const controls = {};
    params.forEach(param => controls[param.displayName] = ['', Validators.required]);
    return controls;
  }

  onSubmit() {
    this.displaySpinner = true;

    const combinedForm = Object.assign(this.overallForm.value, this.painForm.value,
      this.otherForm.value, this.historyForm.value, this.examinationForm.value, this.stomachForm.value);

    this.service.getDiagnosisBySymptoms(combinedForm)
      .subscribe(data => {
        this.displaySpinner = false;
        this.openDialog(data.label, data.helpUrl, data.icd10, data.doctor);
      });
  }

  openDialog(illnessName: string, helpUrl: string, icd10: string, doctor: string): void {
    const dialogRef = this.dialog.open(DiagnoseDialogComponent, {
      width: '40rem',
      data: { diagnose: illnessName, url: helpUrl, icd10: icd10, doctor: doctor }
    });
  }
}
