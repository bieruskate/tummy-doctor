import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class IllnessService {

  constructor(private http: Http) { }

  getDiagnosisBySymptoms(symptoms) {
    return this.http.post('/api/diagnosis', JSON.stringify(symptoms))
      .map(data => data.json());
  }

}
