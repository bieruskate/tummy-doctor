import json
import pickle
from pathlib import Path

import pandas as pd
from sklearn.neural_network import MLPClassifier

DATA_DIR = Path(__file__).parent / 'data'

model_pickle = (DATA_DIR / 'model.pkl').read_bytes()
model: MLPClassifier = pickle.loads(model_pickle)

features = json.loads((DATA_DIR / 'features.json').read_text())
labels = json.loads((DATA_DIR / 'labels.json').read_text())

icd_10 = [
    ('K35.2', 'K35.3', 'K35.8'),
    ('K57.0', 'K57.1', 'K57.2', 'K57.3', 'K57.4', 'K57.5', 'K57.8', 'K57.9',),
    ('K56.0', 'K56.3', 'K56.4', 'K56.5', 'K56.6', 'K56.7'),
    ('K25.1', 'K25.2', 'K25.5', 'K25.6'),
    ('K81.0', 'K81.1', 'K81.8', 'K81.9'),
    ('K85.0', 'K85.1', 'K85.2', 'K85.3', 'K85.8', 'K85.9'),
    ['nieznany'],
    ['nieznany'],
]

doctors = [
    'SOR',
    'gastroenterolog',
    'gastroenterolog',
    'SOR',
    'internista, chirurg',
    'internista, anastezjolog',
    'internista',
    'internista'
]


def predict(item: dict):
    assert set(item.keys()) == set(features)

    df = pd.DataFrame([item])
    df = df[features]
    prediction = model.predict(df)[0]
    return {
        'prediction': int(prediction),
        'label': labels[prediction - 1],
        'icd10': ', '.join(icd_10[prediction - 1]),
        'doctor': doctors[prediction - 1],
    }
