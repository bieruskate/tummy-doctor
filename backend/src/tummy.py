import json

import requests
from bs4 import BeautifulSoup
from flask import Blueprint, Flask, request

from ml_loader import predict

bp = Blueprint('api', __name__)


@bp.route('/')
def index():
    return 'Hello world'


@bp.route('/diagnosis', methods=['POST'])
def get_diagnosis():
    features = json.loads(request.data)
    prediction_dict = predict(features)

    label = prediction_dict['label']
    response = requests.get(f'http://google.pl/search?q={label}')

    if not response.ok:
        return 'Error'

    soup = BeautifulSoup(response.content, 'html.parser')
    help_url = soup.find('h3').find('a').get('href')

    cat_index = help_url.find('&')
    if cat_index != -1:
        help_url = help_url[7:cat_index]
    prediction_dict['helpUrl'] = help_url

    return json.dumps(prediction_dict)


app = Flask(__name__)
app.register_blueprint(bp, url_prefix='/api')

if __name__ == '__main__':
    app.run(debug=True)
