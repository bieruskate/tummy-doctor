import json
from collections import OrderedDict as OrD
from pathlib import Path
from pprint import pprint

fn = Path('../data/questions.json')
text = fn.read_text()
data = json.loads(text)

d1 = {
    group: {
        question: {
            answer: i
            for i, answer in enumerate(answers, idx)
        }
        for question, idx, answers in questions
    }
    for group, questions in data
}

d2 = OrD([
    (group, OrD([
        (question, OrD([
            (answer, i)
            for i, answer in enumerate(answers, idx)
        ]))
        for question, idx, answers in questions
    ]))
    for group, questions in data
])

# pprint(d1)
pprint(d2)
print(d1 == d2)


label = 'Choroba'

features = [j for _, i in data for j, *_ in i]
features.remove(label)

# pprint(features)
# feature_dumps = json.dumps(features, ensure_ascii=False)
# Path('../data/features.json').write_text(feature_dumps)
#
# labels = list(d2[label][label].keys())
#
# pprint(labels)
# feature_dumps = json.dumps(labels, ensure_ascii=False)
# Path('../data/labels.json').write_text(feature_dumps)



def states():
    i = 0
    l = []
    for group, questions in d2.items():
        if group == 'Choroba':
            continue
        for question, answers in questions.items():
            l.append(f'export const q{i} = {{')
            l.append(f'    displayName: {features[i]!r},')
            l.append(f'    options: [')
            for answer, idx in answers.items():
                l.append(f'        {{ value: {idx}, viewValue: {answer.title()!r} }},')
            l.append('    ]')
            l.append('};\n')
            i += 1

    join = '\n'.join(l)
    fn = '../../frontend/src/src/app/illness-form/states.ts'
    Path(fn).write_text(join)


def form():
    l = ['<form [formGroup]="patientForm">']

    '''
  <mat-form-field>
    <mat-select placeholder="Płeć" formControlName="sex">
      <mat-option *ngFor="let sex of sexs" [value]="sex.value">
        {{sex.viewValue}}
      </mat-option>
    </mat-select>
  </mat-form-field>

'''
    i = 0
    for group, questions in d2.items():
        if group == 'Choroba':
            continue
        for question, answers in questions.items():
            l.append(f'''
  <mat-form-field>
    <mat-select placeholder="{question}" formControlName="q{i}">
      <mat-option *ngFor="let q of q{i}" [value]="q.value">
        {{{{q.viewValue}}}}
      </mat-option>
    </mat-select>
  </mat-form-field>''')
            i += 1
    l.append('''
</form>

Form value: {{ patientForm.value | json }}''')
    join = '\n'.join(l)
    fn = '../../frontend/src/src/app/illness-form/illness-form.component.html'
    Path(fn).write_text(join)


if __name__ == '__main__':
    states()
    form()
