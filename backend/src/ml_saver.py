import json
import pickle
from pathlib import Path

import pandas as pd
from sklearn.neural_network import MLPClassifier

DATA_DIR = Path(__file__).parent.parent / 'data'

features = json.loads((DATA_DIR / 'features.json').read_text())

label = 'Choroba'

data = pd.read_csv(DATA_DIR / 'data.csv')
y = data[label]
x = data[features]

data = data[features]

model = MLPClassifier()
model.fit(x, y)

model_pickle = pickle.dumps(model)
(DATA_DIR / 'model.pkl').write_bytes(model_pickle)
